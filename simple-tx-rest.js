var request = require('request');

//Initialize variables
const nodeAddress = 'https://my-iota-node.com:14267'
const address = 'IOTA9ITALIA9MEETUP9MILANO99999999999999999999999999999999999999999999999999999999'
const minWeightMagnitude = 14
const myTxTag = 'IOTA9ITALIA9RULEZ'


var command = {
  'command': 'findTransactions',
  'addresses': ['IOTA9ITALIA9MEETUP9MILANO99999999999999999999999999999999999999999999999999999999']
}

var options = {
  url: nodeAddress,
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
		'X-IOTA-API-Version': '1',
    'Content-Length': Buffer.byteLength(JSON.stringify(command))
  },
  json: command
};

request(options, function (error, response, data) {
  if (!error && response.statusCode == 200) {
    console.log(data);
  }
});