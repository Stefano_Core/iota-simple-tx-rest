const https = require('https')

const data = JSON.stringify({
    'command': 'findTransactions',
    'addresses': ['IOTA9ITALIA9MEETUP9MILANO99999999999999999999999999999999999999999999999999999999']
})

const options = {
  hostname: 'my-iota-node.com',
  port: '14267',
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
	'X-IOTA-API-Version': '1',
    'Content-Length': Buffer.byteLength(JSON.stringify(data))
  }
}

const req = https.request(options, (error, response, receivedData) => {
  if(error){
      console.log(error)
  } else {
    if(response.statusCode){
        console.log(response.statusCode)
    }
  }})

req.write(data)
req.end()